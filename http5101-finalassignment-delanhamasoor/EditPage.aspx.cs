﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_finalassignment_delanhamasoor
{
    public partial class EditPost : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                post.InnerHtml = "Sorry, this post no longer exists. It may never have existed to begin with. Spooky.";
                return;
            }
            post_title.Text = pagerow["pagetitle"].ToString();
            post_author_name.Text = pagerow["pageauthor"].ToString();
            post_content.Text = pagerow["pagecontent"].ToString();
        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select pageid, pagetitle, pageauthor, pagecontent from MyPages where pageid=" + id.ToString();
            edit_post_select.SelectCommand = query;

            DataView pageview = (DataView)edit_post_select.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }

        protected void SaveChanges(object sender, EventArgs e)
        {
            string ptitle = post_title.Text;
            string pname = post_author_name.Text;
            string pcontent = post_content.Text;

            string upquery = "Update MyPages set pagetitle='" + ptitle + "'," +
                "pageauthor='" + pname + "',pagecontent='" + pcontent + "' where pageid=" + pageid;

            update_page.UpdateCommand = upquery;
            update_page.Update();

            Response.Redirect("~/PageManager.aspx");
        }
    }
}