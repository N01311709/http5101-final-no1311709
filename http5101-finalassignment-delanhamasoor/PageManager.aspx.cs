﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_finalassignment_delanhamasoor
{
    public partial class PageManager : Page
    {
        private string sqlline = "SELECT pageid, pagetitle as Post, convert(varchar,datecreated,106) as \"Date Created\", convert(varchar,datepublished,106) as \"Date Published\", isvisible FROM MyPages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_manager_select.SelectCommand = sqlline;
            menuManager_list.DataSource = Manager_Manual_Bind(pages_manager_select);


            BoundColumn p_id = new BoundColumn();
            p_id.DataField = "pageid";
            p_id.HeaderText = "ID";
            p_id.Visible = false;
            menuManager_list.Columns.Add(p_id);

            BoundColumn name = new BoundColumn();
            name.DataField = "Post";
            name.HeaderText = "<strong>Post</strong>";
            menuManager_list.Columns.Add(name);

            BoundColumn created = new BoundColumn();
            created.DataField = "Date Created";
            created.HeaderText = "<strong>Date Created</strong>";
            menuManager_list.Columns.Add(created);

            BoundColumn published = new BoundColumn();
            published.DataField = "Date Published";
            published.HeaderText = "<strong>Date Published</strong>";
            menuManager_list.Columns.Add(published);

            BoundColumn edit = new BoundColumn();
            edit.DataField = "Edit";
            edit.HeaderText = "<strong>Edit</strong>";
            menuManager_list.Columns.Add(edit);

            BoundColumn pagestatus = new BoundColumn();
            pagestatus.DataField = "isvisible";
            pagestatus.HeaderText = "isvisible";
            pagestatus.Visible = false;
            menuManager_list.Columns.Add(pagestatus);
            
            BoundColumn displaystatus = new BoundColumn();
            displaystatus.DataField = "Status";
            displaystatus.HeaderText = "<strong>Status</strong>";
            menuManager_list.Columns.Add(displaystatus);
            
            ButtonColumn pagevis = new ButtonColumn();
            pagevis.HeaderText = "<b>Visibility</b>";
            pagevis.Text = "Toggle Visibility";
            pagevis.ButtonType = ButtonColumnType.PushButton;
            pagevis.CommandName = "VisToggle";
            menuManager_list.Columns.Add(pagevis);
            
            ButtonColumn delpage = new ButtonColumn();
            delpage.HeaderText = "<strong>Delete</strong>";
            delpage.Text = "Delete";
            delpage.ButtonType = ButtonColumnType.PushButton;
            delpage.CommandName = "DelPage";
            menuManager_list.Columns.Add(delpage);

            menuManager_list.DataBind();
        }

        protected void Posts_ItemCommand(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "DelPage")
            {
                int pageid = Convert.ToInt32(e.Item.Cells[0].Text);
                DelPage(pageid);
            }
            
            if (e.CommandName == "VisToggle")
            {
                int pageid = Convert.ToInt32(e.Item.Cells[0].Text);
                string status = Convert.ToString(e.Item.Cells[6].Text);
                ToggleVis(pageid,status);
            }
        }
        
        protected void ToggleVis(int pageid, string isvisvalue)
        {
            int setVis;

            if (isvisvalue == "Hidden")
            {
                setVis = 1;
            } else if (isvisvalue == "Published")
            {
                setVis = 0;
            } else
            {
                return;
            }

            string visquery = "Update MyPages set isvisible=" + setVis + " where pageid=" + pageid;
            toggle_vis.UpdateCommand = visquery;
            toggle_vis.Update();

            Response.Redirect("~/PageManager.aspx");

        }

        protected void DelPage(int pageid)
        {
            string delquery = "DELETE FROM MyPages WHERE pageid=" + pageid.ToString();
            del_page.DeleteCommand = delquery;
            del_page.Delete();

            Response.Redirect("~/PageManager.aspx");
        }

        protected DataView Manager_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Edit";
            mytbl.Columns.Add(editcol);
            
            DataColumn viscol = new DataColumn();
            viscol.ColumnName = "Status";
            mytbl.Columns.Add(viscol);
            
            foreach (DataRow row in mytbl.Rows)
            {
                row["Post"] =
                    "<a href=\"Post.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["Post"]
                    + "</a>";

                row[editcol] = "<a href=\"EditPage.aspx?pageid=" + row["pageid"] + "\">Edit</a>";
                
                if (Convert.ToInt32(row["isvisible"]) == 1)
                {
                    row[viscol] = "Published";
                } else
                {
                    row[viscol] = "Hidden";
                }

            }

            myview = mytbl.DefaultView;

            return myview;
        }

        protected void Search_Titles(object sender, EventArgs e)
        {
            string newsql = sqlline + " WHERE (1=1) ";
            string key = title_search.Text;
            List<string> sec = new List<string>();
            
            if (key != "")
            {
                newsql +=
                    " AND pagetitle LIKE '%" + key + "%'";
            }
            
            pages_manager_select.SelectCommand = newsql;
            
            menuManager_list.DataSource = Manager_Manual_Bind(pages_manager_select);

            menuManager_list.DataBind();

        }

    }
}