﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_finalassignment_delanhamasoor
{
    public partial class NavGen : System.Web.UI.UserControl
    {
        private string sqlline = "SELECT pageid, pagetitle FROM MyPages WHERE isvisible = 1";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_nav_select.SelectCommand = sqlline;
            navPosts_list.DataSource = Pages_Manual_Bind(pages_nav_select);

            BoundColumn p_id = new BoundColumn();
            p_id.DataField = "pageid";
            p_id.HeaderText = "ID";
            p_id.Visible = false;
            navPosts_list.Columns.Add(p_id);

            BoundColumn title = new BoundColumn();
            title.DataField = "pagetitle";
            title.HeaderText = "<h4>Check Out My Posts!";
            navPosts_list.Columns.Add(title);

            navPosts_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["pagetitle"] =
                    "<a href=\"Post.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";
            }
            myview = mytbl.DefaultView;
            return myview;
        }
    }
}