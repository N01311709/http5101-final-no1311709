﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_finalassignment_delanhamasoor
{
    public partial class Post : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                title.InnerHtml = "Sorry, this post no longer exists. It may never have existed to begin with. Spooky.";
                return;
            }
            
            title.InnerHtml = pagerow["pagetitle"].ToString();
            author.InnerHtml = "By " + pagerow["pageauthor"].ToString();
            date.InnerHtml = "Posted " + pagerow["datepublished"].ToString();
            post.InnerHtml = pagerow["pagecontent"].ToString();
        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select pageid, pagetitle, pageauthor, pagecontent, convert(varchar,datepublished,106) as datepublished from MyPages where pageid=" + id.ToString();
            view_pages.SelectCommand = query;
            
            DataView pageview = (DataView)view_pages.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }
    }
}