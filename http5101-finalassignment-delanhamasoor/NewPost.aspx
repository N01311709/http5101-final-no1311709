﻿<%@ Page Title="New Post" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPost.aspx.cs" Inherits="http5101_finalassignment_delanhamasoor.NewPost" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:SqlDataSource runat="server" id="create_page"
        ConnectionString="<%$ ConnectionStrings:pages_sql_db %>">
    </asp:SqlDataSource>

    <h2><%: Title %></h2>
        <div>
            <p>
            <asp:Label ID="labelTitle" runat="server" AssociatedControlID="post_title" Text="Title:"></asp:Label>
            <asp:TextBox id="post_title" runat="server"></asp:TextBox>
            </p>
            <p>
            <asp:Label ID="labelName" runat="server" AssociatedControlID="post_author_name" Text="Author Name:"></asp:Label>
            <asp:TextBox id="post_author_name" runat="server"></asp:TextBox>
            </p>
            <p><asp:Label ID="labelPost" runat="server" AssociatedControlID="post_content" Text="Post:"></asp:Label>
            </p>
            <p><asp:TextBox ID="post_content" runat="server" TextMode="MultiLine" Height="400px" Width="400px"></asp:TextBox>
            </p>
            <asp:Button runat="server" ID="pubButton" OnClick="Publish" Text="Publish"/>
        </div>
</asp:Content>

