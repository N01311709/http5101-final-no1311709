﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace http5101_finalassignment_delanhamasoor
{
    public partial class NewPost : System.Web.UI.Page
    {
        private string pubquery = "INSERT into MyPages " +
        "(pagetitle, pageauthor, pagecontent, datecreated, datepublished, isvisible)" +
        " VALUES ";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Publish(object sender, EventArgs e)
        {
            string ptitle = post_title.Text;
            string pname = post_author_name.Text;
            string pcontent = post_content.Text;
            string pdate = DateTime.Today.ToString("dd/MM/yyyy");

            pubquery += "('" + ptitle + "', '" + pname + "', '" +
                pcontent + "',convert(DATETIME,'" + pdate + "',103),convert(DATETIME,'" + pdate + "',103),1)";

            create_page.InsertCommand = pubquery;
            create_page.Insert();

            Response.Redirect("~/PageManager.aspx");
        }
    }
}