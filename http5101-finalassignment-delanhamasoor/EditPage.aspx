﻿<%@ Page Title="Edit Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="http5101_finalassignment_delanhamasoor.EditPost" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="edit_post_select"
        ConnectionString="<%$ ConnectionStrings:pages_sql_db %>">
    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="update_page"
        ConnectionString="<%$ ConnectionStrings:pages_sql_db %>">
    </asp:SqlDataSource>

    <h2><%: Title %></h2>
        <div id="post" runat="server">
            <asp:Label ID="labelTitle" runat="server" AssociatedControlID="post_title" Text="Title:"></asp:Label>
            <asp:TextBox id="post_title" runat="server"></asp:TextBox>
           
            <asp:Label ID="labelName" runat="server" AssociatedControlID="post_author_name" Text="Author Name:"></asp:Label>
            <asp:TextBox id="post_author_name" runat="server"></asp:TextBox>
            
            <asp:Label ID="labelPost" runat="server" AssociatedControlID="post_content" Text="Post:"></asp:Label>
            <asp:TextBox ID="post_content" runat="server" TextMode="MultiLine" Height="400px" Width="400px"></asp:TextBox>
         
            <asp:Button runat="server" ID="pubButton" OnClick="SaveChanges" Text="Save and Publish"/>
      </div>
</asp:Content>