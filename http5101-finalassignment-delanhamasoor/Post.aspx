﻿<%@ Page Title="My Blog Post" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Post.aspx.cs" Inherits="http5101_finalassignment_delanhamasoor.Post" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="view_pages"
        ConnectionString="<%$ ConnectionStrings:pages_sql_db %>">
    </asp:SqlDataSource>

    <div runat="server" id="debug"></div>

    <h2 id="title" runat="server"></h2>
    <h3 id="author" runat="server"></h3>
    <h3 id="date" runat="server"></h3>
    <div id="post" runat="server"></div>
</asp:Content>
