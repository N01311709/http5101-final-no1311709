﻿<%@ Page Title="Page Manager" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PageManager.aspx.cs" Inherits="http5101_finalassignment_delanhamasoor.PageManager" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>


    <div>
        <asp:Label ID="labelSearch" runat="server" AssociatedControlID="title_search" Text="Search Titles:"></asp:Label>
        <asp:TextBox runat="server" id="title_search" width="300px"></asp:TextBox>
        <asp:Button runat="server" Text="Search" OnClick="Search_Titles"/>
    </div>

    <div id="new_post">
        <a runat="server" href="~/NewPost">Add a new page</a>
    </div>

    <asp:SqlDataSource runat="server" id="pages_manager_select" 
                ConnectionString="<%$ ConnectionStrings:pages_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:pages_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server"
        id="toggle_vis"
        ConnectionString="<%$ ConnectionStrings:pages_sql_db %>"></asp:SqlDataSource>

    <asp:DataGrid 
        id="menuManager_list" 
        CssClass="pageManager" 
        runat="server" 
        AutoGenerateColumns="false" 
        OnItemCommand="Posts_ItemCommand">
    </asp:DataGrid>

    <div id="debug" runat="server"></div>



</asp:Content>
